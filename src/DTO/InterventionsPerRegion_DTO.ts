import { Region } from "../model/Region";

export class InterventionsPerRegion_DTO{
  region: string
  numberOfInterventions: number

  constructor(region: string,numberOfInterventions: number)
  {
    this.region = region;
    this.numberOfInterventions = numberOfInterventions
  }
}
