export class InterventionsPerPeriod_DTO{
  numberOfInterventions: number
  period: string

  constructor(numberOfInterventions: number,period: string)
  {
    this.numberOfInterventions = numberOfInterventions;
    this.period = period
  }
}
