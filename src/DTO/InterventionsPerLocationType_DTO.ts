import { InterventionType } from "../model/InterventionType";

export class InterventionsPerLocationType_DTO{
  location: string
  numberOfInterventions: number

  constructor(location: string,numberOfInterventions: number)
  {
    this.location = location;
    this.numberOfInterventions = numberOfInterventions
  }
}
