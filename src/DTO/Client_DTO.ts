import { Client } from "../model/Client";

export class Client_DTO{
  client: Client
  numberOfInterventions: number
  hoursOfInterventions: number

  constructor(client: Client,numberOfInterventions: number,hoursOfInterventions: number)
  {
    this.client = client;
    this.numberOfInterventions = numberOfInterventions
    this.hoursOfInterventions
  }
}
