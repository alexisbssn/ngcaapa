import { Theme } from "../model/Theme";

export class InterventionsPerTheme_DTO{
  theme: string
  numberOfInterventions: number

  constructor(theme: string,numberOfInterventions: number)
  {
    this.theme = theme;
    this.numberOfInterventions = numberOfInterventions
  }
}
