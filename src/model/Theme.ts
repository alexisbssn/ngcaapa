export class Theme{
  id: number;
  label: string;
  description: string;

  constructor(label, description) {
    this.label = label;
    this.description = description;
  }
}
