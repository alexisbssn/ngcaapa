import { Ressource } from "./Ressource";
import { Theme } from "./Theme";
import { Document } from "./Document";
import { Intervention } from "./Intervention"
import { Demande } from "./Demande"

export class Probleme{
  id: number;
  description: string;
  status: string;
  recommendation: string;

  theme: Theme;
  documents: Array<Document>;
  demande: Demande;
  problemes: Array<Probleme>;
  interventions: Array<Intervention>;
  createdBy: Ressource;

  constructor()
  {

  }
}
