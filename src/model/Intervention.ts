import { Probleme } from "./Probleme"
import { Demande } from "./Demande"
import { Ressource } from "./Ressource"
import { InterventionType } from "./InterventionType"

export class Intervention{
  id: number;
  description: string;
  limitDate: string;
  duration: number; //minutes
  distance: number; //km, double
  address: string;
  type: InterventionType;

  problemes: Array<Probleme>;
  demande: Demande;
  createdBy: Ressource;
  ressources: Array<Ressource>;

  constructor(){
  }
}
