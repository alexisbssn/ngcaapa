export class Region{
  id: number;
  regionName: string;

  constructor(regionName) {
    this.regionName = regionName;
  }
}
