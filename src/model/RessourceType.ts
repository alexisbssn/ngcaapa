export class RessourceType{
  id: number;
  label: string;

  constructor(label) {
    this.label = label;
  }
}
