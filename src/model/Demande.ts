import { Reference } from "./Reference"
import { Ressource } from "./Ressource"
import { Client } from "./Client"

export class Demande{
  id: number;
  description: string;
  status: string;

  reference : Reference;
  createdBy : Ressource;
  assignee : Ressource;
  client : Client;

  constructor()
  {
    
  }

}
