import { Probleme } from "./Probleme"

export class Document{
  id: number;
  title: string;
  description: string;

  problemes: Array<Probleme>;

  constructor(title, description) {
    this.title = title;
    this.description = description;
  }
}
