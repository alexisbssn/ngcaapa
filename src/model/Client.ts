import { Contact } from "./Contact";
import { Region } from "./Region";
import { Demande } from "./Demande";

export class Client extends Contact{
  idclient: number;
  sex: string;
  isAlone: boolean;
  maritalStatus: string;
  birthday: string;
  typeHabitation: string;
  enAttenteHabitation: string;

  region : Region;
  contactReference1 : Contact;
  contactReference2 : Contact;


  constructor(){
    super();
  }

}
