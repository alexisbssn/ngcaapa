export class Contact{
  id: number;
  lastName: string;
  firstName: string;
  address: string;
  phone: string;
  phone2: string;
  email: string;

  constructor(){

  }

}
