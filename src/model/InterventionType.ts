export class InterventionType{
  id: number;
  label: string;
  description: string;
  lieu: string;

  constructor(label, description, lieu){
    this.label = label;
    this.description = description;
    this.lieu = lieu;
  }
}
