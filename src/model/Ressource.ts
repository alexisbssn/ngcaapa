import { Contact } from "./Contact";
import { RessourceType } from "./RessourceType";
import { Region } from "./Region";

export class Ressource extends Contact{
  id: number;
  login: string;
  hash: string;

  createdBy: Ressource;
  ressourceType: RessourceType;
  region: Region;

  constructor() {
    super();
  }

}
