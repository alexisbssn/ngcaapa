export class CalendarEvent{
  title: string;
  start: string;
  end: string;
  id: number;
}
