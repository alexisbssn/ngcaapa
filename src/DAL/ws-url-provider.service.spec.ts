/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { WsUrlProviderService } from './ws-url-provider.service';

describe('Service: WsUrlProvider', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [WsUrlProviderService]
    });
  });

  it('should ...', inject([WsUrlProviderService], (service: WsUrlProviderService) => {
    expect(service).toBeTruthy();
  }));
});
