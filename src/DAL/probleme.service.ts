import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod, Request, Jsonp } from '@angular/http';
import { WsUrlProviderService } from './ws-url-provider.service';
import { Observable } from 'rxjs/Rx';
import { Probleme } from '../model/Probleme';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ProblemeService {

  serviceType: string;

  constructor(public http: Http, public jsonp : Jsonp,  public urlProvider : WsUrlProviderService) {
    this.serviceType = "ProblemeService";
  }

  list()
  {
    return this.http.get(this.urlProvider.url + "/problem?PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  load(id : number)
  {
    return this.http.get(this.urlProvider.url + "/problem/?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  create(probleme : Probleme)
  {

    let body = JSON.stringify({probleme});
    console.log(body);
    let headers = new Headers({"Content-Type":"application/json"});
    let options = new RequestOptions({headers : headers});
  //  console.log(this.jsonp.get(this.urlProvider.url + "/problem?PWD=" + this.urlProvider.wsPassword, options));
    this.http.post(this.urlProvider.url + "/problem?PWD=" + this.urlProvider.wsPassword, body, options).map(res => res.json()).subscribe(data => console.log(data));
    //return this.http.post(this.urlProvider.url + "/problem?PWD=" + this.urlProvider.wsPassword, body, options)
    //  .map(res => res.json());

  }

  update(probleme : Probleme){
    let body = JSON.stringify(probleme);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "put" });
    return this.http.put(this.urlProvider.url + "/problem?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }
}
