/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProblemeService } from './probleme.service';

describe('Service: Probleme', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProblemeService]
    });
  });

  it('should ...', inject([ProblemeService], (service: ProblemeService) => {
    expect(service).toBeTruthy();
  }));
});
