import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Contact } from '../../model/Contact';
@Injectable()
export class ContactService {
  serviceType: string;
  constructor(private http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "ContactService";
  }

  getContacts() {
      return this.http.get(this.urlProvider.url)
                  .toPromise()
                  .then(res => <any[]> res.json().data)
                  .then(data => { return data; });
  }

  list(){
    return this.http.get(this.urlProvider.url + "/contact?PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  load(id: number){
    return this.http.get(this.urlProvider.url + "/contact?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  create(contact: Contact){
    let body = JSON.stringify(contact);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/contact?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  update(contact: Contact){
    let body = JSON.stringify(contact);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/contact?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  delete(id:number){
    return this.http.delete(this.urlProvider.url + "/contact?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

}
