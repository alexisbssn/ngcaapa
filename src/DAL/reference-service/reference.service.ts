import { Injectable } from '@angular/core';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Reference } from '../../model/reference';

@Injectable()
export class ReferenceService {

  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "ReferenceService";
  }

  Create(reference: Reference){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(reference);
    let request = new RequestOptions({ headers: head, method: "post" });
    return this.http.post(this.urlProvider.url + "/reference?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  read(id: number){
    let url: string = this.urlProvider.url + "/reference?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  readAll(){
    let url: string = this.urlProvider.url + "/reference?PWD=" + this.urlProvider.wsPassword;
    console.log(url);
    return this.http.get(url).map((res:Response) => res.json());
  }

  update(reference: Reference){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(reference);
    let request = new RequestOptions({ headers: head, method: "post" });
    return this.http.post(this.urlProvider.url + "/reference?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  delete(id: number){
    let url: string = this.urlProvider.url + "/reference?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.delete(url).map((res:Response) => res.json());
  }
}
