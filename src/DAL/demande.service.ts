import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WsUrlProviderService } from './ws-url-provider.service';
import { Observable } from 'rxjs/Rx';
import { Demande } from '../model/Demande';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class DemandeService {

  serviceType: string;

  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "DemandeService";
  }

  list()
  {
    return this.http.get(this.urlProvider.url + "/demande?PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  load(id : number)
  {
    return this.http.get(this.urlProvider.url + "/demande/?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  create(demande : Demande)
  {

    demande.createdBy = this.urlProvider.ressourceCreatedBy;

    let body = JSON.stringify(demande);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/demande?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());

  }

  update(demande: Demande){
    let body = JSON.stringify(demande);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "put" });
    return this.http.put(this.urlProvider.url + "/demande?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }


}
