import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Client } from '../model/Client';
import { Contact } from '../model/Contact';
import { Demande } from '../model/Demande';
import { Document } from '../model/Document';
import { Intervention } from '../model/Intervention';
import { InterventionType } from '../model/InterventionType';
import { Probleme } from '../model/Probleme';
import { Reference } from '../model/Reference';
import { Region } from '../model/Region';
import { Ressource } from '../model/Ressource';
import { RessourceType } from '../model/RessourceType';
import { Theme } from '../model/Theme';

@Injectable()
export class InMemoryDAO{

  protected clients: Array<Client>;
  protected contacts: Array<Contact>;
  protected demandes: Array<Demande>;
  protected documents: Array<Document>;
  protected interventions: Array<Intervention>;
  protected interventionTypes: Array<InterventionType>;
  protected problemes: Array<Probleme>;
  protected reference: Array<Reference>;
  protected regions: Array<Region>;
  protected ressources: Array<Ressource>;
  protected ressourceTypes: Array<RessourceType>;
  protected themes: Array<Theme>;

  constructor(){
    this.demandes = new Array<Demande>();
    this.interventions = new Array<Intervention>();
    this.clients = new Array<Client>();
    this.ressources = new Array<Ressource>();
    this.regions = new Array<Region>();
    this.contacts = new Array<Contact>();

    var client1 = new Client();
    var client2 = new Client();
    var client3 = new Client();

    var ressource1 = new Ressource();
    var ressource2 = new Ressource();

    var region1 = new Region("Montreal");
    var region2 = new Region("Suroit");
    var region3 = new Region("Montérégie");

    var contact1 = new Contact()
    var contact2 = new Contact();
    var contact3 = new Contact();

    var ress = new Ressource();
    ress.login = "bob";

    var ress2 = new Ressource();
    ress2.login = "fleur";

    var d = new Demande();
    d.id = 1234;
    d.description = "Voisin méchant";
    d.assignee = ress;

    var e = new Demande();
    e.id = 1235;
    e.description = "Facture Hydro Québec";
    e.assignee = ress;

    var f = new Demande();
    f.id = 1236;
    f.description = "Le chien du voin est bruyant et le voisin est condescendant et je teste la longueur du champs description...";
    f.assignee = ress2;

    var i = new Intervention();
    i.id = 4321;
    i.description = "Appel téléphonique chez HQ";
    i.limitDate = "2016-11-24";
    i.ressources = [ress];

    var j = new Intervention();
    j.id = 4322;
    j.description = "Rencontre avec le voisin méchant";
    j.limitDate = "2016-11-14";
    j.ressources = [ress];

    var k = new Intervention();
    k.id = 4323;
    k.description = "Shotgun plus chien paf le chien";
    k.limitDate = "2016-11-27"
    k.ressources = [ress];

    this.interventions.push(i);
    this.interventions.push(j);
    this.interventions.push(k);
    this.demandes.push(d);
    this.demandes.push(f);
    this.demandes.push(e);
    this.clients.push(client1);
    this.clients.push(client2);
    this.clients.push(client3);
    this.regions.push(region1);
    this.regions.push(region2);
    this.regions.push(region3);
    this.contacts.push(contact1);
    this.contacts.push(contact2);
    this.contacts.push(contact3);
  }

  getClients(){
    return Observable.from(this.clients);
  }
  getContacts(){
    return Observable.from(this.contacts);
  }
  getDemandes(){
    return Observable.from(this.demandes);
  }
  getDocuments(){
    return null;
  }
  getInterventions(){
    return Observable.from(this.interventions);
  }
  getInterventionTypes(){
    return null;
  }
  getProblemes(){
    return null;
  }
  getReferences(){
    return null;
  }
  getRegions(){
    return Observable.from(this.regions);
  }
  getRessources(){
    return Observable.from(this.ressources);
  }
  getRessourceTypes(){
    return null;
  }
  getTheme(){
    return null;
  }
}
