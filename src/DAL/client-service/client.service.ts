import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Client } from '../../model/Client';
@Injectable()
export class ClientService {
  serviceType: string;
  constructor(private http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "ClientService";
  }

  list(){
    return this.http.get(this.urlProvider.url + "/client?PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  load(id: number){
    return this.http.get(this.urlProvider.url + "/client?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  create(client: Client){
    let body = JSON.stringify(client);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/client?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  update(client: Client){
    let body = JSON.stringify(client);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/client?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  delete(id:number){
    return this.http.delete(this.urlProvider.url + "/client?id=" + id + "&PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }
}
