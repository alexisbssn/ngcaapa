import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'

/*@Injectable()
export class RegionServiceService {

  constructor(public http: Http) { }
  getRegions() {
    let url: string = "";
      return this.http.get(url)
      //.map((res:Response) => res.json());
                  .toPromise()
                  .then(res => <any[]> res.json().data)
                  .then(data => { return data; });
  }
}*/
@Injectable()
export class RegionService {

    constructor(private http: Http) {}

    getRegions() {
        return this.http.get('showcase/resources/data/regions.json')
                    .toPromise()
                    .then(res => <any[]> res.json().data)
                    .then(data => { return data; });
    }
}
