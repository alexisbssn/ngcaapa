import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WsUrlProviderService } from './ws-url-provider.service';
import { Intervention } from '../model/Intervention';

@Injectable()
export class InterventionService {
// à Ajouter dans chaque service
  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "InterventionService";
  }

  list(){
    return this.http.get(this.urlProvider.url + "/intervention?PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }
// Utiliser les mêmes noms de méthodes pour le CRUD
  load(id: number){
    return this.http.get(this.urlProvider.url + "/intervention/?id=" + id + "PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }

  create(intervention: Intervention){
    let body = JSON.stringify(intervention);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/intervention?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  update(intervention: Intervention){
    let body = JSON.stringify(intervention);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let options = new RequestOptions({ headers: headers, method: "put" });
    return this.http.put(this.urlProvider.url + "/intervention?PWD=" + this.urlProvider.wsPassword, body, options)
      .map(res => res.json());
  }

  delete(id: number){
    return this.http.delete(this.urlProvider.url + "/intervention/?id=" + id + "PWD=" + this.urlProvider.wsPassword)
      .map((res: Response) => res.json());
  }
}
