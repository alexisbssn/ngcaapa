/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RessourceService } from './ressource.service';

describe('Service: Ressource', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RessourceService]
    });
  });

  it('should ...', inject([RessourceService], (service: RessourceService) => {
    expect(service).toBeTruthy();
  }));
});
