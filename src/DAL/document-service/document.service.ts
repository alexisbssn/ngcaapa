import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Document } from '../../model/Document';

@Injectable()
export class DocumentService {
  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "DocumentService";
  }

  Create(document: Document){
    let body = JSON.stringify(document);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let request = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/document?PWD=" + this.urlProvider.wsPassword, body, request)
      .map(res => res.json());
  }

  read(id: number){
    let url: string = this.urlProvider.url + "/document?id=" + id + "&PWD=" +this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  readAll(){
    let url: string = this.urlProvider.url + "/document?PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  update(document: Document){
    let body = JSON.stringify(document);
    let headers = new Headers({"Content-Type": "application/JSON"});
    let request = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/document?PWD=" + this.urlProvider.wsPassword, body, request)
      .map(res => res.json());
  }

  delete(id: number){
    let url: string = this.urlProvider.url + "/document?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.delete(url).map((res:Response) => res.json());
  }
}
