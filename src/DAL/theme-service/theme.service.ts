import { Injectable } from '@angular/core';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Theme } from '../../model/theme';

@Injectable()
export class ThemeService {

  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "ThemeService";
  }

  Create(theme: Theme){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(theme);
    let request = new RequestOptions({ headers: head, method: "post" });
    console.log(json);
    return this.http.post(this.urlProvider.url + "/theme?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  read(id: number){
    let url: string = this.urlProvider.url + "/theme?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  readAll(){
    let url: string = this.urlProvider.url + "/theme?PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  update(theme: Theme){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(theme);
    let request = new RequestOptions({ headers: head, method: "post" });
    return this.http.post(this.urlProvider.url + "/theme?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  delete(id: number){
    let url: string = this.urlProvider.url + "/theme?id=" + id + this.urlProvider.wsPassword;
    return this.http.delete(url).map((res:Response) => res.json());
  }
}
