/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RessourceTypeService } from './ressource-type.service';

describe('Service: RessourceType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RessourceTypeService]
    });
  });

  it('should ...', inject([RessourceTypeService], (service: RessourceTypeService) => {
    expect(service).toBeTruthy();
  }));
});
