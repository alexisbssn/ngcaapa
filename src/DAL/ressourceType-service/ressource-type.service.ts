import { Injectable } from '@angular/core';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { RessourceType } from '../../model/ressourceType';

@Injectable()
export class RessourceTypeService {

  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "RessourceTypeService";
  }

  Create(ressourceType: RessourceType){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(ressourceType);
    let request = new RequestOptions({ headers: head, method: "post" });
    return this.http.post(this.urlProvider.url + "/ressourcetype?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  read(id: number){
    let url: string = this.urlProvider.url + "/ressourcetype?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  readAll(){
    let url: string = this.urlProvider.url + "/ressourcetype?PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  update(ressourceType: RessourceType){
    let head = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(ressourceType);
    let request = new RequestOptions({ headers: head, method: "post" });
    return this.http.post(this.urlProvider.url + "/ressourcetype?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  delete(id: number){
    let url: string = this.urlProvider.url + "/ressourcetype?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.delete(url).map((res:Response) => res.json());
  }
}
