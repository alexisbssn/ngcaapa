import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Client } from '../model/Client'
import { Client_DTO} from '../DTO/Client_DTO'
import { InterventionsPerRegion_DTO} from '../DTO/InterventionsPerRegion_DTO'

@Injectable()
export class ChartsService {

  constructor(public http: Http) { }

  getInterventionPerLocation(){
    let url: string = "http://caapavalleyfieldws.azurewebsites.net/api/statistics/getAllStatisticOfLocations";
    return this.http.get(url).map((res:Response) => res.json())
  }

  getInterventionPerPeriod(flag:string){
    let url: string = "http://caapavalleyfieldws.azurewebsites.net/api/statistics/getStatisticsForPeriod?periodFlag="+flag;
    return this.http.get(url).map((res:Response) => res.json())
  }

  getInterventionPerRegion(){
    let url: string = "http://caapavalleyfieldws.azurewebsites.net/api/statistics/getAllStatisticOfRegions";
    return this.http.get(url).map((res:Response) => res.json())
  }

  getInterventionPerTheme(){
    let url: string = "http://caapavalleyfieldws.azurewebsites.net/api/statistics/getAllStatisticOfThemes";
    return this.http.get(url).map((res:Response) => res.json())
  }

  getClientInformationByID(id:number){
    let url: string = "http://caapavalleyfieldws.azurewebsites.net/api/statistics/getStatisticsForAClient?clientid="+id;
    return this.http.get(url).map((res:Response) => res.json())
  }
}
