import { Injectable } from '@angular/core';
import { WsUrlProviderService } from '../ws-url-provider.service';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { InterventionType } from '../../model/InterventionType';

@Injectable()
export class InterventionTypeService {

  serviceType: string;
  constructor(public http: Http, public urlProvider : WsUrlProviderService) {
    this.serviceType = "InterventionTypeService";
  }

  Create(interventionType: InterventionType){
    let headers = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(interventionType);
    let request = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/interventiontype?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  read(id: number){
    let url: string = this.urlProvider.url + "/interventiontype?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  readAll(){
    let url: string = this.urlProvider.url + "/interventiontype?PWD=" + this.urlProvider.wsPassword;
    return this.http.get(url).map((res:Response) => res.json());
  }

  update(interventionType: InterventionType){
    let headers = new Headers({"Content-Type": "application/JSON"});
    let json = JSON.stringify(interventionType);
    let request = new RequestOptions({ headers: headers, method: "post" });
    return this.http.post(this.urlProvider.url + "/interventiontype?PWD=" + this.urlProvider.wsPassword, json, request)
      .map(res => res.json());
  }

  delete(id: number){
    let url: string = this.urlProvider.url + "/interventiontype?id=" + id + "&PWD=" + this.urlProvider.wsPassword;
    return this.http.delete(url).map((res:Response) => res.json());
  }
}
