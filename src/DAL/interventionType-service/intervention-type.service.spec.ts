/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InterventionTypeService } from './intervention-type.service';

describe('Service: InterventionType', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InterventionTypeService]
    });
  });

  it('should ...', inject([InterventionTypeService], (service: InterventionTypeService) => {
    expect(service).toBeTruthy();
  }));
});
