//side NAVBAR
$(document).ready(function() {
  var isOpen = false;
  var isMobile = false;

  var navbarDiv = $("#navbarDiv");
  var contentDiv = $("#contentDiv");
  var navbar = $("#navbar");
  var allNavbarlinkSections = navbar.find(".navbarLinkSection");
  var allSublink = allNavbarlinkSections.find(".navbarSublink");
  var allMenusIcon = allNavbarlinkSections.find(".menuIcon");
  var allMenusText = allNavbarlinkSections.find(".menuText");

  //Add bootstarp to columns in navbar
  //mobile && Desktop
  allMenusText.addClass("col-xs-10 visible-xs");
  allMenusIcon.addClass("col-xs-2 col-sm-12");
  allSublink.hide();

//les a directement dans un navbarLinkSection suivis de navbarSublink
//doivent avoir un <span class="buttonSublink">e</span> dans menuText
  allNavbarlinkSections.each( function( ) {
    var link = $(this).find("a").first();
    //if sibling of sublink
    if(link.next().length){
      var menuText = link.find(".menuText");
      menuText.append('<a class="buttonSublink"><i class="fa fa-chevron-down" aria-hidden="true"></i></a>');

      var buttonlink = menuText.find("a");
      var navbarSublink = link.next();
      buttonlink.on( "click", function() {
        if( navbarSublink.is(":hidden") ){
          event.preventDefault();
          event.stopPropagation();
          navbarSublink.show();
        }
        else{
          event.preventDefault();
          event.stopPropagation();
          navbarSublink.hide();
        }
      });
    }
  });

  onResize();
  $( window ).resize(function() {
    onResize();
  });

  //Show menuText onHover
  navbar.hover(function() {
    if(!isMobile && !isOpen){
      allMenusText.removeClass("visible-xs");
      allMenusText.find("p").parent().addClass("menuAbsolute");
    }
  }, function(){
    if(!isMobile && !isOpen){
      allMenusText.find("p").parent().removeClass("menuAbsolute");
      allMenusText.addClass("visible-xs");
      hideAllSubmenu();
    }
  });

  //Show menuText onClick
  $("#displayNavBar").click(function() {
    if(isMobile){
      if(isOpen){
        //close it
        closeMobileMenu();
      }
      else{
        //open it
        var headerMargin = $("#header").css("margin-right").replace(/[^-\d\.]/g, '');
        var headerWidthNMargin = $("#header").css("width").replace(/[^-\d\.]/g, '');
        var headerWidthOnly = +headerWidthNMargin + +(headerMargin*2);

        navbar.addClass("mobileMenuAbsolute");
        navbar.css("width", headerWidthOnly+"px");
        navbarDiv.show();
        isOpen = true;
      }
    }
    else{
      if(isOpen){
        //close it
        reverseDesktop();
        isOpen = false;
      }
      else{
        //open it
        navbarDiv.removeClass("col-sm-1").addClass("col-sm-3");
        contentDiv.removeClass("col-sm-11").addClass("col-sm-9");

        allMenusIcon.removeClass("col-sm-12").addClass("col-sm-4");
        allMenusText.removeClass("visible-xs").addClass("col-sm-8");

        isOpen = true;
      }
    }
  });

  function onResize(){
    //isMobile
    if($(window).width() < 768){
      //changed to mobile
      if(!isMobile) {
        isOpen = false;
        isMobile = true;
        reverseDesktop();
        navbar.find('a[routerLink^="/"]').bind( "click", closeMobileMenu );
      }
    }
    else{
      //changed to desktop
      if(isMobile) {
        isOpen = false;
        isMobile = false;
        reverseMobile();
        navbar.find('a[routerLink^="/"]').unbind( "click", closeMobileMenu );
      }
    }

    //hide/show navbar
    if(isMobile){
      navbarDiv.hide();
    }
    else{
      navbarDiv.show();
    }
  }
  function reverseDesktop(){
    //revese the desktop menu effect if screen change when menu is open
    navbarDiv.removeClass("col-sm-3").addClass("col-sm-1");
    contentDiv.removeClass("col-sm-9").addClass("col-sm-11");

    allMenusIcon.removeClass("col-sm-4").addClass("col-sm-12");
    allMenusText.removeClass("col-sm-8").addClass("visible-xs");

    hideAllSubmenu();
  }
  function reverseMobile(){
    //reverse the mobile menu effect if screen change when menu is open
    hideAllSubmenu();
    navbar.removeClass("mobileMenuAbsolute");
    navbar.css("width","");

  }
  var closeMobileMenu = function (){
    reverseMobile();
    navbarDiv.hide();
    isOpen = false;
  }

  function hideAllSubmenu(){
    allNavbarlinkSections.each( function( ) {
      var link = $(this).find("a").first();
      //if sibling of sublink
      if(link.next().length && link.next().is(":visible")){
        link.next().hide();
      }
    });
  }
});

//headerNavbar
$(document).ready(function(){
  var statistiqueLinks = $("#statistiqueLinks");
  var appTitle = $("#appTitle");
  statistiqueLinks.hide();
  appTitle.show();

  var links = $("a[routerLink^=\"/\"]");
  links.click(function() {
    statistiqueLinks.hide();
    appTitle.show();
  });

  var link = $('a[routerLink="/chart-intervention-per-location"]');
  link.click(function() {
    statistiqueLinks.show();
    appTitle.hide();
  });

  if($("#statistique").length){
    statistiqueLinks.show();
    appTitle.hide();
  }
});
