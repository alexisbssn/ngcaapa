import { Component, OnInit } from '@angular/core';
import {ConfirmDialogModule,ConfirmationService} from 'primeng/primeng';
@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css'],
  providers: [ConfirmationService]
})
export class DeleteComponent implements OnInit {

  // msgs: Message[] = [];

  constructor(private confirmationService: ConfirmationService) { }

  ngOnInit() {
  }

  confirmDelete() {
        // this.confirmationService.confirm({
        //     message: 'Êtes-vous sûre de vouloir supprimer cet élément?',
        //     header: 'Confirmation de suppression',
        //     icon: 'fa fa-trash',
        //     accept: () => {
        //         this.msgs = [];
        //         this.msgs.push({severity:'info', summary:'Confimation', detail:'Élément supprimé'});
        //     }
        // });
    }
}
