import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { InputTextModule, PasswordModule } from 'primeng/primeng';
import { WsUrlProviderService } from "../../DAL/ws-url-provider.service";
import { RessourceService } from '../../DAL/ressource-service/ressource.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(private webservice : WsUrlProviderService, private ressourceService : RessourceService) { }

  ngOnInit() {
  }

  @Output() onLogin = new EventEmitter<boolean>();

  //TODO: disable text fields during WS call to log in (current WS does not accept logon request)
  isLoggingIn = false;

  login() {
  //  this.webservice.RessourceCreatedBy = this.ressourceService.getRessourceByLogin(this.webservice.username);
    this.onLogin.emit(true);
  }

}
