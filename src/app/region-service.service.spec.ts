/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RegionServiceService } from './region-service.service';

describe('Service: RegionService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RegionServiceService]
    });
  });

  it('should ...', inject([RegionServiceService], (service: RegionServiceService) => {
    expect(service).toBeTruthy();
  }));
});
