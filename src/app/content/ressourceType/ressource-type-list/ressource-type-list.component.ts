import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { RessourceType } from '../../../../model/RessourceType';
import { RessourceTypeService } from '../../../../DAL/ressourceType-service/ressource-type.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-ressource-type-list',
  templateUrl: './ressource-type-list.component.html',
  styleUrls: ['./ressource-type-list.component.css']
})
export class RessourceTypeListComponent implements OnInit {

  @Input() values: Array<any>;
  value: any[];

  ressourceTypes: Array<RessourceType>;

  constructor(private ressourceTypeService: RessourceTypeService) { }

  ngOnInit() {
    this.ressourceTypeService.readAll().subscribe((values : Array<any>) => this.ressourceTypes = values);
  }
}
