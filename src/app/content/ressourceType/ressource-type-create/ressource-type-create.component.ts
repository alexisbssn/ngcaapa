import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RessourceTypeService } from '../../../../DAL/ressourceType-service/ressource-type.service';
import { RessourceType } from '../../../../model/RessourceType';

@Component({
  selector: 'app-ressource-type-create',
  templateUrl: './ressource-type-create.component.html',
  styleUrls: ['./ressource-type-create.component.css']
})
export class RessourceTypeCreateComponent implements OnInit {

  labelRessourceType : string = "";

  @Output() ressourceTypeUpdated = new EventEmitter<RessourceType>()
  newRessourceType : RessourceType;

  constructor(private ressourceTypeService: RessourceTypeService) {
    this.ressourceTypeUpdated.emit(this.newRessourceType);
  }

  ngOnInit() {
  }

  CreateARessourceType(){
    var ressourceType : RessourceType = new RessourceType(this.labelRessourceType);
    this.ressourceTypeService.Create(ressourceType);
  }
}
