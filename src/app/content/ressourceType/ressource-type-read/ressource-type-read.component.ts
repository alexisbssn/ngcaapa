import { Component, OnInit } from '@angular/core';
import { RessourceTypeService } from '../../../../DAL/ressourceType-service/ressource-type.service';
import { ActivatedRoute } from '@angular/router';
import { RessourceType } from '../../../../model/RessourceType';

@Component({
  selector: 'app-ressource-type-read',
  templateUrl: './ressource-type-read.component.html',
  styleUrls: ['./ressource-type-read.component.css']
})
export class RessourceTypeReadComponent implements OnInit {

  ressourceType: RessourceType;

  constructor(private route: ActivatedRoute, private ressourceTypeService: RessourceTypeService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.ressourceTypeService.read(id).subscribe(response => { this.ressourceType = response;  });
    });
  }

}
