/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { RegionReadComponent } from './region-read.component';

describe('RegionReadComponent', () => {
  let component: RegionReadComponent;
  let fixture: ComponentFixture<RegionReadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegionReadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegionReadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
