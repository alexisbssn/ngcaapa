import { Component, OnInit } from '@angular/core';
import { RegionService } from '../../../../DAL/region-service/region.service';
import { ActivatedRoute } from '@angular/router';
import { Region } from '../../../../model/Region';

@Component({
  selector: 'app-region-read',
  templateUrl: './region-read.component.html',
  styleUrls: ['./region-read.component.css']
})
export class RegionReadComponent implements OnInit {

  region: Region;

  constructor(private route: ActivatedRoute, private regionService: RegionService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.regionService.read(id).subscribe(response => { this.region = response;});
    });
  }

}
