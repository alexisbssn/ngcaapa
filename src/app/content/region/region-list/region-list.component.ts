import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Region } from '../../../../model/Region';
import { RegionService } from '../../../../DAL/region-service/region.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-region-list',
  templateUrl: './region-list.component.html',
  styleUrls: ['./region-list.component.css']
})
export class RegionListComponent implements OnInit {

  @Input() values: Array<any>;
  isNewRegion: boolean;
  selectedRegion: Region;

  value: any[];

  regions: Array<Region>;

  constructor(private regionService: RegionService) { }

  ngOnInit() {
    this.regionService.readAll().subscribe((values : Array<any>) => this.regions = values);
  }
}
