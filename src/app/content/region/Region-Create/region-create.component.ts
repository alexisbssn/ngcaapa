import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { RegionService } from '../../../../DAL/region-service/region.service';
import { Region } from '../../../../model/Region';

@Component({
  selector: 'app-region-create',
  templateUrl: './region-create.component.html',
  styleUrls: ['./region-create.component.css']
})
export class RegionCreateComponent implements OnInit {

  nameRegion : string = "";

  @Output() regionUpdated = new EventEmitter<Region>()
  newRegion : Region;

  constructor(private regionService: RegionService) {
    this.regionUpdated.emit(this.newRegion);
  }

  ngOnInit() {
  }

  CreateARegion(){
    var region : Region = new Region(this.nameRegion);
    this.regionService.Create(region);
  }
}
