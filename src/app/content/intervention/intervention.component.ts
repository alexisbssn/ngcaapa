import { Component, OnInit } from '@angular/core';
import { Intervention } from '../../../model/Intervention' ;
import { InterventionType } from '../../../model/InterventionType';
import { InterventionService } from '../../../DAL/intervention.service';

import {SelectItem} from 'primeng/primeng';

@Component({
  selector: 'app-intervention',
  templateUrl: './intervention.component.html',
  styleUrls: ['./intervention.component.css']
})
export class InterventionComponent implements OnInit {

  newIntervention: Intervention;
  //displayDialog: boolean = false;

  allTypes: SelectItem[];
  selectedType: string;


  displayDialog: boolean;
  intervention: PrimeIntervention;

  isNewIntervention: boolean;
  selectedIntervention: Intervention;

  interventions: Array<Intervention>;
  dataIntervention: any[];
  cols: any[];

  date1: Date;

  constructor(private interventionService: InterventionService) {

    // this.allTypes = [];
    // this.allTypes.push({label:'Selectionner un type', value:null});
    // this.allTypes.push({label:'Collecte de données au bureau', value:{id: 1, label:"Collecte de données", description:"Établissement du premier contact avec le client", lieu: "Bureau"}})
    // this.allTypes.push({label:'Évaluation à domicile', value:{id: 2, label:"Évaluation", description:"Évaluation de la situation", lieu: "Domicile"}})
  }

  ngOnInit() {
    //this.interventionService.getInterventions().subscribe((data) => this.dataIntervention = data)
    this.interventions = [
    //   new Intervention(1,"description de l'intervention","2016/12/06",0,0,"Adresse de l'intervention",
    //   new InterventionType(1,"Collecte de données","Établissement du premier contact avec le client","Bureau"),
    // null, null, [],[])
    ];

    this.cols = [
      {field: 'description',  header: 'Description'},
      {field: 'limitDate',    header: 'Date limite'},
      {field: 'duration',     header: 'Durée (Minute)'},
      {field: 'distance',     header: 'Distance (Km)'},
      {field: 'address',      header: "Adresse de l'intervention"},
      {field: 'type.label',   header: "Type d'intervention"}
    ];
  }


  onRowSelect(event) {
    this.isNewIntervention = false;
    this.intervention = this.cloneIntervention(event.data);
    // this.displayDialog = true;
}

save() {
    if(this.isNewIntervention)
        this.interventions.push(this.intervention);
    else
        this.interventions[this.findSelectedInterventionIndex()] = this.intervention;

    this.intervention = null;
    // this.displayDialog = false;
}

delete() {
    this.interventions.splice(this.findSelectedInterventionIndex(), 1);
    this.intervention = null;
    // this.displayDialog = false;
}
findSelectedInterventionIndex(): number {
    return this.interventions.indexOf(this.selectedIntervention);
}

cloneIntervention(i: Intervention): Intervention {
    let intervention = new PrimeIntervention(null, null, null, null, null, null, null, null, null, [], []);
    for(let prop in i) {
        intervention[prop] = i[prop];
    }
    return intervention;
}

}

class PrimeIntervention implements Intervention {
   constructor(public id, public description, public limitDate, public distance, public duration, public address,
   public type, public createdBy, public demande, public ressources, public problemes ){}
 }
