import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Intervention } from '../../../../model/Intervention';

@Component({
  selector: 'app-intervention-create',
  templateUrl: './intervention-create.component.html',
  styleUrls: ['./intervention-create.component.css']
})
export class InterventionCreateComponent implements OnInit {
  /// à ajouter dans chaque service
  @Output() interventionUpdated = new EventEmitter<Intervention>()

  newIntervention : Intervention;

  constructor() {
    this.newIntervention = new Intervention();
    this.interventionUpdated.emit(this.newIntervention);
  }

  ngOnInit() {
  }


}
