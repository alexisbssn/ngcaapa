import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';
import { Ressource } from '../../../../model/Ressource';
import { Contact } from '../../../../model/Contact';
import { Region } from '../../../../model/Region';
import { RessourceType } from '../../../../model/RessourceType';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-ressource-create',
  templateUrl: './ressource-create.component.html',
  styleUrls: ['./ressource-create.component.css']
})
export class RessourceCreateComponent implements OnInit {
  @Output() ressourceUpdated = new EventEmitter<Ressource>()

  regions:Array<Region>;
  createdBys:Array<Contact>;
  ressourceTypes:Array<RessourceType>;

  newRessource : Ressource;

  constructor() {
    this.ressourceUpdated.emit(this.newRessource);
    this.regions = [new Region("Montreal"),
                     new Region("Suroit"),
                     new Region("Montérégie")
                   ];
    this.createdBys = [new Ressource(),
                    new Ressource()
                  ];
    this.ressourceTypes = [new RessourceType("Avocat"),
                     new RessourceType("Intervenant"),
                     new RessourceType("Voisin")
                   ]
  }

  ngOnInit() {
  }

  toCreateRessource(){

  }

}
