import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Ressource } from '../../../../model/Ressource';
import { RessourceService } from '../../../../DAL/ressource-service/ressource.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-ressource-list',
  templateUrl: './ressource-list.component.html',
  styleUrls: ['./ressource-list.component.css']
})
export class RessourceListComponent implements OnInit {

  @Input() values: Array<any>;
  newRessource: Ressource;
  allTypes: SelectItem[];
  selectedType: string;

  displayDialog: boolean;
  ressource: PrimeRessource;

  isNewRessource: boolean;
  selectedRessource: Ressource;

  ressources: Array<Ressource>;
  dataRessource: any[];
  value: any[];

  date1: Date;

  constructor(private ressourceService: RessourceService) { }

  ngOnInit() {
    this.ressourceService.list().subscribe((values : Array<any>) => this.value = values);

    this.ressources = [
    ];

    /*this.value = [
      {field: 'lastName',   header: 'Nom'},
      {field: 'firstName',  header: 'Prenom'},
      {field: 'address',    header: 'Adresse'},
      {field: 'phone',      header: 'Telephone'},
      {field: 'phone2',     header: 'Telephone2'},
      {field: 'email',      header: 'Email'},
      {field: 'login',      header: 'Nom utilisateur'},
      {field: 'createdBy',      header: 'Créé par'},
      {field: 'ressourceType',  header: 'Type de ressource'},
      {field: 'region',         header: 'Region'}
    ];*/

  }

  onRowSelect(event) {
    this.isNewRessource = false;
    this.ressource = this.cloneRessource(event.data);
  }

  save() {
      if(this.isNewRessource)
          this.ressources.push(this.ressource);
      else
          this.ressources[this.findSelectedRessourceIndex()] = this.ressource;

      this.ressource = null;
  }

  delete() {
      this.ressources.splice(this.findSelectedRessourceIndex(), 1);
      this.ressource = null;
  }
  findSelectedRessourceIndex(): number {
      return this.ressources.indexOf(this.selectedRessource);
  }

  cloneRessource(r: Ressource): Ressource {
      let ressource = new PrimeRessource(null, null, null, null, null, null, null, null, null, null, null, null);
      for(let prop in r) {
          ressource[prop] = r[prop];
      }
      return ressource;
  }
}

class PrimeRessource implements Ressource {
    constructor(public id, public lastName, public firstName, public address, public phone, public phone2, public email,
    public login, public hash, public createdBy, public ressourceType, public region){}
}
