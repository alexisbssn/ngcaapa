import { Component, OnInit, Inject } from '@angular/core';
import { InterventionService } from '../../../DAL/intervention.service';
import { InMemoryDAO } from '../../../DAL/InMemoryDAO';
import { Intervention } from '../../../model/Intervention';
import { Ressource } from '../../../model/Ressource';
import { WsUrlProviderService } from '../../../DAL/ws-url-provider.service';

@Component({
  selector: 'app-my-interventions',
  templateUrl: './my-interventions.component.html',
  styleUrls: ['./my-interventions.component.css']
})
export class MyInterventionsComponent implements OnInit {

  interventions: Array<any>;

  constructor(private dao: InterventionService, private wsProvider: WsUrlProviderService){
    this.interventions = new Array<any>();
  }

//TODO: Move logic to InterventionService
  ngOnInit() {
    this.dao.list().subscribe((items) => {
      var intervention: Intervention;// = items;

      for(intervention of items){
        var ressource: Ressource;
        for(ressource of intervention.ressources){
            if(ressource.login == this.wsProvider.username){
              this.interventions.push(intervention);
              break;
            }
        }
      }
    });
  }

}
