import { Component, OnInit, Input } from '@angular/core';
import { Demande } from '../../../model/Demande';
import { Intervention } from '../../../model/Intervention';
import { DataTableModule, SharedModule } from 'primeng/primeng';

@Component({
  selector: 'app-assigned-list',
  templateUrl: './assigned-list.component.html',
  styleUrls: ['./assigned-list.component.css']
})
export class AssignedListComponent implements OnInit {

  @Input() values: Array<any>;

  constructor() {
  }

  ngOnInit() {
  }
}
