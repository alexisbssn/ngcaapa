import { Component, OnInit, Input, Output, OnChanges, EventEmitter, trigger, state, style, animate, transition } from '@angular/core';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';

import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import { Probleme } from '../../../../model/Probleme'
import { Theme } from '../../../../model/Theme'
import { DemandeService } from '../../../../DAL/demande.service'
import { ProblemeService } from '../../../../DAL/probleme.service'
import { ThemeService } from '../../../../DAL/theme-service/theme.service'

@Component({
  selector: 'app-create-edit-probleme',
  templateUrl: './create-edit-probleme.component.html',
  styleUrls: ['./create-edit-probleme.component.css']
})
export class CreateEditProblemeComponent implements OnInit {

  @Output() problemeUpdated = new EventEmitter<Probleme>();
  newProbleme : Probleme = new Probleme();

  statusList: SelectItem[];
  selectedStatus: string;

  currentTheme: Theme;
  themes: SelectItem[];

  currentDemande: Demande;
  demandes: SelectItem[];

  constructor(public demandeService: DemandeService, public themeService : ThemeService, public problemeService : ProblemeService)
  {
    this.newProbleme.description = "";
    this.newProbleme.recommendation = "";
    this.statusList = [];
    this.themes = [];
    this.demandes = [];
  }

  ngOnInit()
  {
    this.fillDatasources();
  }

  fillStatus()
  {
    this.statusList = [];
    this.statusList.push({label: 'Choisir status', value: null});
    this.statusList.push({label:'Ouverte', value:"Ouverte"});
    this.statusList.push({label:'En cours', value:"En cours"});
    this.statusList.push({label:'Fermée', value:"Fermée"});
  }

  fillDemandes()
  {
    this.demandeService.list().subscribe((items) => {
      var demandeItems = items;

      for(var demande of demandeItems)
      {
        var newDemande = new Demande();
        newDemande.id = demande.iddemande;
        newDemande.description = demande.description;
        this.demandes.push({label: "#" + newDemande.id + " - " + newDemande.description, value: newDemande});
      }
      console.log(this.demandes)

    });
  }

  fillThemes()
  {
    this.themeService.readAll().subscribe((items) => {
      var themeItems = items;

      for(var theme of themeItems)
      {
        var newTheme = new Theme(theme.label,theme.description);
        newTheme.id = theme.idtheme;
        this.themes.push({label: theme.label, value: newTheme});
      }
      console.log(this.themes);

    });
  }

  fillDatasources()
  {
    this.fillStatus();
    this.fillDemandes();
    this.fillThemes();
  }



  sendInformations()
  {
    this.newProbleme.status = this.selectedStatus;
    this.newProbleme.theme = this.currentTheme;
    this.newProbleme.documents = [];
    this.newProbleme.demande = this.currentDemande;
    this.newProbleme.problemes = [];
    this.newProbleme.interventions = [];
    this.newProbleme.createdBy = new Ressource();
    this.newProbleme.createdBy.id = 23;
    this.problemeService.create(this.newProbleme);
  }

}
