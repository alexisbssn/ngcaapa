import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';
import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Probleme } from '../../../../model/Probleme'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import {ProblemeService } from '../../../../DAL/probleme.service'

@Component({
  selector: 'app-probleme',
  templateUrl: './probleme.component.html',
  styleUrls: ['./probleme.component.css']
})
export class ProblemeComponent implements OnInit {
  problemes: Array<any>;
  selectedProbleme : Probleme;

  constructor(public problemeService: ProblemeService)
  {

  }

  ngOnInit() {
    this.problemeService.list().subscribe((items) => {
      this.problemes = items;
      console.log(this.problemes)

    });
  }

  onRowSelect(event) {
    console.log(this.selectedProbleme);
    // this.displayDialog = true;
}

}
