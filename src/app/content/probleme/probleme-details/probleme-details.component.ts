import { Component, OnInit, Inject } from '@angular/core';

import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import { Probleme } from '../../../../model/Probleme'
import { ProblemeService } from '../../../../DAL/probleme.service'
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-probleme-details',
  templateUrl: './probleme-details.component.html',
  styleUrls: ['./probleme-details.component.css']
})
export class ProblemeDetailsComponent implements OnInit {

  id: number;
  private sub: any;

  probleme : Probleme;

  constructor(private route: ActivatedRoute, public problemeService: ProblemeService)
  {

  }


  fillProbleme()
  {
    this.problemeService.load(this.id).subscribe((items) => {
      this.probleme = items;
      console.log(this.probleme);
    });

  }

  fillDatasources()
  {
    this.fillProbleme();

  }

ngOnInit() {
  this.sub = this.route.params.subscribe(params => {
     this.id = +params['id']; // (+) converts string 'id' to a number

     // In a real app: dispatch action to load the details here.
  });
  console.log(this.id);
  this.fillDatasources();
  console.log(this.probleme);
  }
}
