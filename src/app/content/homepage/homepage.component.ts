import { Component, OnInit, Inject } from '@angular/core';
import { ScheduleModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { InMemoryDAO } from '../../../DAL/InMemoryDAO';
import { Intervention } from '../../../model/Intervention';
import { CalendarEvent } from '../../../model/CalendarEvent';

@Component({
  selector: 'homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

    overviewItems: Array<any>;
    calendarEvents: Array<CalendarEvent>;

    constructor(private dao: InMemoryDAO){
      this.overviewItems = new Array<any>();
      this.calendarEvents = new Array<CalendarEvent>();
    }
    ngOnInit(){
      //TODO: lorsque la logique de my-demandes et my-interventions sera dans les service, utiliser ici.
      this.dao.getDemandes().subscribe((items) => this.overviewItems.push(items));
      this.dao.getInterventions().subscribe((items) => {
        var interventions = new Array<any>();
        interventions.push(items);
        var i: Intervention
        for(i of interventions){
          var ce = new CalendarEvent();
          ce.title = i.description;
          ce.start = i.limitDate;
          ce.id = i.id;
          this.calendarEvents.push(ce);
        }

        this.overviewItems.push(items);
      });
    }
}
