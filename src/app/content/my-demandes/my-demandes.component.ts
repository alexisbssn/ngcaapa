import { Component, OnInit, Inject } from '@angular/core';
import { InMemoryDAO } from '../../../DAL/InMemoryDAO';
import { Demande } from '../../../model/Demande';
import { WsUrlProviderService } from '../../../DAL/ws-url-provider.service';

@Component({
  selector: 'app-my-demandes',
  templateUrl: './my-demandes.component.html',
  styleUrls: ['./my-demandes.component.css']
})
export class MyDemandesComponent implements OnInit {

  demandes: Array<any>;

  constructor(private dao: InMemoryDAO, private wsProvider: WsUrlProviderService){
    this.demandes = new Array<any>();
  }

  ngOnInit() {
    this.dao.getDemandes().subscribe((items) => {
      var demande: Demande = items;
      //for(demande of items){
        if(demande.assignee.login == this.wsProvider.username){
          this.demandes.push(demande)
        }
      //}
    });
  }
}
