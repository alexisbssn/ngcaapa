import { Component, OnInit, Input, Output, OnChanges, EventEmitter, trigger, state, style, animate, transition } from '@angular/core';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';

import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import { DemandeService } from '../../../../DAL/demande.service'
import { ClientService } from '../../../../DAL/client-service/client.service'
import { ReferenceService} from '../../../../DAL/reference-service/reference.service'
import { RessourceService } from '../../../../DAL/ressource-service/ressource.service'

@Component({
  selector: 'app-create-edit-demande',
  templateUrl: './create-edit-demande.component.html',
  styleUrls: ['./create-edit-demande.component.css']
})
export class CreateEditDemandeComponent implements OnInit {

        @Output() demandeUpdated = new EventEmitter<Demande>();
        newDemande : Demande = new Demande();

        statusList: SelectItem[];
        selectedStatus: string;

        currentClient: Client;
        clients: SelectItem[];

        currentReference: any;
        references: any[];
        currentReferences: any[];

        currentAssignee: Ressource;
        assignees: SelectItem[];


        filterReferencesEvent(event) {

        let query = event.query;
        this.currentReferences = this.filterReferences(query, this.references);

        }

        filterReferences(query, references: any[]):any[] {
        let filtered : any[] = [];
        for(let i = 0; i < references.length; i++) {
            let reference = references[i];
            if(reference.description.toLowerCase().indexOf(query.toLowerCase()) == 0) {
                filtered.push(reference);
            }
        }
        return filtered;
    }

        constructor(public demandeService: DemandeService, public ressourceService : RessourceService, public clientService : ClientService, public referenceService :  ReferenceService)
        {
          this.newDemande.description = "";
          this.assignees = [];
          this.references = [];
          this.clients = [];
        }

        ngOnInit()
        {
          this.fillDatasources();
        }

        fillStatus()
        {
          this.statusList = [];
          this.statusList.push({label: 'Choisir status', value: null});
          this.statusList.push({label:'Ouverte', value:"Ouverte"});
          this.statusList.push({label:'En cours', value:"En cours"});
          this.statusList.push({label:'Fermée', value:"Fermée"});
        }

        fillReferences()
        {
          this.referenceService.readAll().subscribe((items) => {
            var referenceItems = items;

            for(var reference of referenceItems)
            {
              var newReference = new Reference(reference.description);
              newReference.id = reference.idreference;
              this.references.push(newReference);
            }
            console.log(this.references)

          });
        }

        fillAssignees()
        {
          this.ressourceService.list().subscribe((items) => {
            var ressourceItems = items;

            for(var ressource of ressourceItems)
            {
              if(ressource.ressourcetype.label == "Interne")
              {
              var newRessource = new Ressource();
              newRessource.id = ressource.idressource;
              newRessource.firstName = ressource.contact.firstName;
              newRessource.lastName = ressource.contact.lastName;
              this.assignees.push({label: newRessource.firstName + " " + newRessource.lastName, value: newRessource});
              }
            }
            console.log(this.assignees)

          });
        }

        fillClients()
        {
          this.clientService.list().subscribe((items) => {
            var clientItems = items;

            for(var client of clientItems)
            {
              var newClient = new Client();
              newClient.idclient = client.idclient;
              newClient.firstName = client.contact.firstName;
              newClient.lastName = client.contact.lastName;
              this.clients.push({label: newClient.firstName + " " + newClient.lastName, value: newClient});
            }
            console.log(this.clients)

          });
        }

        fillDatasources()
        {
          this.fillStatus();
          this.fillAssignees();
          this.fillClients();
          this.fillReferences();
        }

        sendInformations()
        {
          this.newDemande.status = this.selectedStatus;
          this.newDemande.reference = this.currentReference;
          this.newDemande.client = this.currentClient;
          this.newDemande.assignee = this.currentAssignee;
          this.demandeService.create(this.newDemande);
        }

}
