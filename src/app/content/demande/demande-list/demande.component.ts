import { Component, OnInit, Inject, Output, EventEmitter } from '@angular/core';

import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import { DemandeService } from '../../../../DAL/demande.service'


@Component({
  selector: 'app-demande',
  templateUrl: './demande.component.html',
  styleUrls: ['./demande.component.css']
})

export class DemandeComponent implements OnInit {
  demandes: Array<any>;
  selectedDemande : Demande;

  constructor(public demandeService: DemandeService)
  {
    this.demandes = Array<Demande>();
  //  this.newDemande = new Demande();
  //  this.demandeUpdated.emit(this.newDemande);
  }

  onRowSelect(event) {
    console.log(this.selectedDemande);
    // this.displayDialog = true;
}

  ngOnInit() {
    this.demandeService.list().subscribe((items) => {
      this.demandes = items;
      console.log(this.demandes)

    });


  }

}
