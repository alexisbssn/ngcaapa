import { Component, OnInit, Inject } from '@angular/core';

import { Demande } from '../../../../model/Demande'
import { Client } from '../../../../model/Client'
import { Reference } from '../../../../model/Reference'
import { Ressource } from '../../../../model/Ressource'
import { Probleme } from '../../../../model/Probleme'
import { DemandeService } from '../../../../DAL/demande.service'
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-demande-details',
  templateUrl: './demande-details.component.html',
  styleUrls: ['./demande-details.component.css']
})
export class DemandeDetailsComponent implements OnInit {
    id: number;
    private sub: any;

    demande : Demande;
    problemes : Array<any>;


  constructor(private route: ActivatedRoute, public demandeService: DemandeService)
  {
    this.problemes = [];
  }


  fillProblemes()
  {
    //  this.problemeService.list().subscribe((items) => this.problemes.push(items));
  }

  fillDemande()
  {
    console.log(this.id);
    this.demandeService.load(this.id).subscribe((items) => {
      this.demande = items;
    });
  }

  fillDatasources()
  {
    this.fillProblemes();
    this.fillDemande();
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
       this.id = +params['id']; // (+) converts string 'id' to a number

       // In a real app: dispatch action to load the details here.
    });
    console.log(this.id);
    this.fillDatasources();

  }

}
