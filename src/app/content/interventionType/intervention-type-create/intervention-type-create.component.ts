import { Component, OnInit,Input, Output, EventEmitter } from '@angular/core';
import { InterventionTypeService } from '../../../../DAL/interventionType-service/intervention-type.service';
import { InterventionType } from '../../../../model/InterventionType';

@Component({
  selector: 'app-intervention-type-create',
  templateUrl: './intervention-type-create.component.html',
  styleUrls: ['./intervention-type-create.component.css']
})
export class InterventionTypeCreateComponent implements OnInit {
  @Output() interventionTypeUpdated = new EventEmitter<InterventionType>()

  newInterventionType : InterventionType;

  constructor(private interventionTypeService: InterventionTypeService) {
    this.interventionTypeUpdated.emit(this.newInterventionType);
  }

  labelIntType : string = "";
  descriptionIntType : string = "";
  lieuIntType : string = "";

  ngOnInit() {
  }

  CreateAInterventionType(){
    var interventionType : InterventionType = new InterventionType(this.labelIntType, this.descriptionIntType, this.lieuIntType);
    this.interventionTypeService.Create(interventionType);
  }
}
