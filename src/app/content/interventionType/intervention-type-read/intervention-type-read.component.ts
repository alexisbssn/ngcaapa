import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { InterventionTypeService } from '../../../../DAL/interventionType-service/intervention-type.service';
import { InterventionType } from '../../../../model/InterventionType';

@Component({
  selector: 'app-intervention-type-read',
  templateUrl: './intervention-type-read.component.html',
  styleUrls: ['./intervention-type-read.component.css']
})
export class InterventionTypeReadComponent implements OnInit {

  interventionType: InterventionType;

  constructor(private route: ActivatedRoute, private interventionTypeService: InterventionTypeService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.interventionTypeService.read(id).subscribe(response => { this.interventionType = response;  });
    });
  }

}
