import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { InterventionType } from '../../../../model/InterventionType';
import { InterventionTypeService } from '../../../../DAL/interventionType-service/intervention-type.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-intervention-type-list',
  templateUrl: './intervention-type-list.component.html',
  styleUrls: ['./intervention-type-list.component.css']
})
export class InterventionTypeListComponent implements OnInit {

  @Input() values: Array<any>;
  interventionType: InterventionType;

  interventionTypes: Array<InterventionType>;

  constructor(private interventionTypeService: InterventionTypeService) { }

  ngOnInit() {
    this.interventionTypeService.readAll().subscribe((values : Array<any>) => this.interventionTypes = values);
  }

  onRowSelect(event) {
    this.interventionType = event.data;
  }

}
