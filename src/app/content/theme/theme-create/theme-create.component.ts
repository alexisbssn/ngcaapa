import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ThemeService } from '../../../../DAL/theme-service/theme.service';
import { Theme } from '../../../../model/Theme';

@Component({
  selector: 'app-theme-create',
  templateUrl: './theme-create.component.html',
  styleUrls: ['./theme-create.component.css']
})
export class ThemeCreateComponent implements OnInit {

  labelTheme : string = "";
  descriptionTheme : string = "";

  @Output() themeUpdated = new EventEmitter<Theme>()
  newTheme : Theme;

  constructor(private themeService: ThemeService) {
      this.themeUpdated.emit(this.newTheme);
  }

  ngOnInit() {
  }

  CreateATheme(){
    var theme : Theme = new Theme(this.labelTheme, this.descriptionTheme);
    this.themeService.Create(theme);
  }
}
