import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Theme } from '../../../../model/Theme';
import { ThemeService } from '../../../../DAL/theme-service/theme.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-theme-list',
  templateUrl: './theme-list.component.html',
  styleUrls: ['./theme-list.component.css']
})
export class ThemeListComponent implements OnInit {

  cols;
  value: any[];
  themes: Array<Theme>;

  constructor(private themeService: ThemeService) { }

  ngOnInit() {
    this.themeService.readAll().subscribe(response => this.themes = response);
  }
}
