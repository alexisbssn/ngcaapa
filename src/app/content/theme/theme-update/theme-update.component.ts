import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../../../DAL/theme-service/theme.service';
import { ActivatedRoute } from '@angular/router';
import { Theme } from '../../../../model/Theme';

@Component({
  selector: 'app-theme-update',
  templateUrl: './theme-update.component.html',
  styleUrls: ['./theme-update.component.css']
})
export class ThemeUpdateComponent implements OnInit {

  theme: Theme;

  constructor(private route: ActivatedRoute, private themeService: ThemeService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.themeService.read(id).subscribe(response => { this.theme = response;  });
    });
  }
}
