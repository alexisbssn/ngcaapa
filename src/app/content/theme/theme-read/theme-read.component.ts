import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../../../DAL/theme-service/theme.service';
import { ActivatedRoute } from '@angular/router';
import { Theme } from '../../../../model/Theme';

@Component({
  selector: 'app-theme-read',
  templateUrl: './theme-read.component.html',
  styleUrls: ['./theme-read.component.css']
})
export class ThemeReadComponent implements OnInit {

  theme: Theme;

  constructor(private route: ActivatedRoute, private themeService: ThemeService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.themeService.read(id).subscribe(response => { this.theme = response;  });
    });
  }

}
