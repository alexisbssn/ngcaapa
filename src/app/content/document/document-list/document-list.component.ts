import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Document } from '../../../../model/Document';
import { DocumentService } from '../../../../DAL/document-service/document.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css']
})
export class DocumentListComponent implements OnInit {

  @Input() values: Array<any>;
  displayDialog: boolean;

  documents: Array<Document>;

  value: any[];

  date1: Date;

  constructor(private documentService: DocumentService) { }

  ngOnInit() {
    this.documentService.readAll().subscribe((values : Array<any>) => this.documents = values);
  }
}
