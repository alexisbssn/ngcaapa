import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DocumentService } from '../../../../DAL/document-service/document.service';

@Component({
  selector: 'app-document-read',
  templateUrl: './document-read.component.html',
  styleUrls: ['./document-read.component.css']
})
export class DocumentReadComponent implements OnInit {

  document: Document;

  constructor(private route: ActivatedRoute, private documentService: DocumentService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.documentService.read(id).subscribe(response => { this.document = response; });
    });
  }

}
