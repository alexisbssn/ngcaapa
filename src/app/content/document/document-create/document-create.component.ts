import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DocumentService } from '../../../../DAL/document-service/document.service';
import { Document } from '../../../../model/Document';

@Component({
  selector: 'app-document-create',
  templateUrl: './document-create.component.html',
  styleUrls: ['./document-create.component.css']
})
export class DocumentCreateComponent implements OnInit {
  @Output() documentUpdated = new EventEmitter<Document>()
  newdocument : Document;

  titleDocument: string = "";
  descriptionDocument: string = "";

  constructor(private docService: DocumentService) {
    this.documentUpdated.emit(this.newdocument);
  }

  ngOnInit() {
  }

  CreateADocument(){
    var doc : Document = new Document(this.titleDocument, this.descriptionDocument);
    this.docService.Create(doc);
  }

}
