import { Component, OnInit } from '@angular/core';
import { ReferenceService } from '../../../../DAL/reference-service/reference.service';
import { ActivatedRoute } from '@angular/router';
import { Reference } from '../../../../model/Reference';

@Component({
  selector: 'app-reference-read',
  templateUrl: './reference-read.component.html',
  styleUrls: ['./reference-read.component.css']
})
export class ReferenceReadComponent implements OnInit {

  reference: Reference;

  constructor(private route: ActivatedRoute, private referenceService: ReferenceService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id'];
       this.referenceService.read(id).subscribe(response => { this.reference = response;  });
    });
  }

}
