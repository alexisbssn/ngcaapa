import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Reference } from '../../../../model/Reference';
import { ReferenceService } from '../../../../DAL/reference-service/reference.service';
import { SelectItem } from 'primeng/primeng';

@Component({
  selector: 'app-reference-list',
  templateUrl: './reference-list.component.html',
  styleUrls: ['./reference-list.component.css']
})
export class ReferenceListComponent implements OnInit {

  @Input() values: Array<any>;
  newReference: Reference;
  displayDialog: boolean;


  references: Array<Reference>;

  value: any[];

  constructor(private referenceService: ReferenceService) { }

  ngOnInit() {
    this.referenceService.readAll().subscribe((values : Array<any>) => this.references = values);
  }
}
