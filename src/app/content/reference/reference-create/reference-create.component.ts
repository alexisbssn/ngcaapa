import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ReferenceService } from '../../../../DAL/reference-service/reference.service';
import { Reference } from '../../../../model/Reference';

@Component({
  selector: 'app-reference-create',
  templateUrl: './reference-create.component.html',
  styleUrls: ['./reference-create.component.css']
})
export class ReferenceCreateComponent implements OnInit {

  descriptionRef: string = "";

  @Output() referenceUpdated = new EventEmitter<Reference>()
  newReference : Reference;

  constructor(private refService: ReferenceService) {
    this.referenceUpdated.emit(this.newReference);
  }

  ngOnInit() {
  }

  CreateAReference(){
    var reference : Reference = new Reference(this.descriptionRef);
    this.refService.Create(reference);
  }
}
