import { Component, OnInit, Input, Inject } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';
import { Client } from '../../../../model/Client';
import { ClientService } from '../../../../DAL/client-service/client.service';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css']
})
export class ClientListComponent implements OnInit {

  allTypes: SelectItem[];
  selectedType: string;

  displayDialog: boolean;
  client: Client;

  isNewClient: boolean;
  selectedClient: Client;
  clients: Array<Client>;

  dataClient: any[];
  cols: any[];

  date1: Date;

  constructor(private dao: ClientService) {
  }

  ngOnInit() {
    this.dao.list().subscribe((items) => this.clients = items);

    this.cols = [
      {field: 'lastName',   header: 'Nom'},
      {field: 'firstName',  header: 'Prenom'},
      {field: 'address',    header: 'Adresse'},
      {field: 'phone',      header: 'Telephone'},
      {field: 'phone2',     header: 'Telephone2'},
      {field: 'email',      header: 'Email'},
      {field: 'sex',        header: 'Sexe'},
      {field: 'isAlone',    header: 'Vie seul'},
      {field: 'maritalStatus',      header: 'Statue marital'},
      {field: 'birthday',           header: 'Date de fete'},
      {field: 'typeHabitation',     header: 'Type habitation'},
      {field: 'enAttenteHabitation',header: 'En attente habitation'}
    ];

  }

  onRowSelect(event) {
    this.isNewClient = false;
    this.client = event.data;
  }

  save() {
      if(this.isNewClient)
          this.clients.push(this.client);
      else
          this.clients[this.findSelectedClientIndex()] = this.client;

      this.client = null;
  }

  delete() {
      this.clients.splice(this.findSelectedClientIndex(), 1);
      this.client = null;
  }
  findSelectedClientIndex(): number {
      return this.clients.indexOf(this.selectedClient);
  }
}
