import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ClientService } from '../../../../DAL/client-service/client.service';
import { Client } from '../../../../model/Client';

@Component({
  selector: 'app-client-details',
  templateUrl: './client-details.component.html',
  styleUrls: ['./client-details.component.css']
})
export class ClientDetailsComponent implements OnInit {

  client: Client;

  constructor(private route: ActivatedRoute, private dao: ClientService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id']; // (+) converts string 'id' to a number
       this.dao.load(id).subscribe(response => {
         this.client = response;
       });
    });
  }

}
