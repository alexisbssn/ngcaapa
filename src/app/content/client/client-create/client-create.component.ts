import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { Client } from '../../../../model/Client';
import { ClientService } from '../../../../DAL/client-service/client.service';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';
import { Region } from '../../../../model/Region';
import { Contact } from '../../../../model/Contact';
import { Ressource } from '../../../../model/Ressource';
import { InMemoryDAO } from '../../../../DAL/InMemoryDAO';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({

  selector: 'app-client-create',
  templateUrl: './client-create.component.html',
  styleUrls: ['./client-create.component.css']
})

export class ClientCreateComponent implements OnInit {

  clientLastName :string;
  clientPrenom :string;
  clientAdresse :string;
  clientTel1 :string;
  clientTel2 :string;
  clientEmail :string;
  clientSexe: string;
  clientMaritalStatus: string;
  clientBirthday: any;
  clientTypeHabitation: string;
  clientEnAttenteHabitation: string;
  clientRegion: string;
  clientContactUrgence: string;

  @Output() clientUpdated = new EventEmitter<Client>()
  newClient : Client;

  statusList: SelectItem[];
  selectedStatus: string;

  currentClient: Client;

  currentReference: any;
  references: any[];
  currentReferences: any[];
  assignees: Array<any>;

  texts: string[];
  results: string[];
  dateWithIcon: Date;
  minDate: Date;
  maxDate: Date;
  es: any;
  clients: Array<any>;
  regions:Array<Region>;
  contactsUrgence:Array<Contact>;
  constructor(private dao: InMemoryDAO){
    this.clients = new Array<any>();
    /*this.clientUpdated.emit(this.newClient);
    this.regions = [new Region("Montreal"),
                     new Region("Suroit"),
                     new Region("Montérégie")
                   ];
    this.contactsUrgence = [new Ressource("Graton", "Bob","Rue du sacre","4505551234","","bgraton@hotmail.com"),
                    new Ressource("Dilan", "Boby","Rue de la musique","4505551234","","bdylan@hotmail.com")
                  ];*/
  }

  toCreateClient(){

    console.log("Je suis dans la méthode toCreateClient");
    var client = new Client();
    /*this.clients.push({label:'', value:{
      name:this.clientLastName,
      firstName:this.clientPrenom,
      adresse:this.clientAdresse,
      phone:this.clientTel1,
      phone2:this.clientTel2,
      email:this.clientEmail,
      sex:this.clientSexe,
      maritalStatus:this.clientMaritalStatus,
      birthday:this.clientBirthday,
      typeHabitation: this.clientTypeHabitation,
      enAttenteHabitation: this.clientEnAttenteHabitation,
      region: this.clientRegion,
      contactUrgence: this.clientContactUrgence
    }});*/
  }

  ngOnInit() {
    this.dao.getClients().subscribe((items) => this.clients.push(items));

    this.es = {
          firstDayOfWeek: 1,
          dayNames: [ "Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi" ],
          dayNamesShort: [ "dim","lun","mar","mer","jeu","ven","sam" ],
          dayNamesMin: [ "D","L","M","M","J","V","S" ],
          monthNames: [ "Janvier","Fevrier","Mars","Avril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre" ],
          monthNamesShort: [ "jan","feb","mar","avr","mai","jui","jui","aou","sep","oct","nov","dec" ]
      }

      let today = new Date();
      let month = today.getMonth();
      let prevMonth = (month === 0) ? 11 : month -1;
      let nextMonth = (month === 11) ? 0 : month + 1;
      this.minDate = new Date();
      this.minDate.setMonth(prevMonth);
      this.maxDate = new Date();
      this.maxDate.setMonth(nextMonth);
  }

}
