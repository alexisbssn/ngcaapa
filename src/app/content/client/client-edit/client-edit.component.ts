import { Component, OnInit } from '@angular/core';
import { Client } from '../../../../model/Client';
import { Contact } from '../../../../model/Contact';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})

export class ClientEditComponent implements OnInit {

  contactUrgencearr: Array<Contact>;
  constructor() {
    this.contactUrgencearr = []
  }

  ngOnInit() {
  }

}
