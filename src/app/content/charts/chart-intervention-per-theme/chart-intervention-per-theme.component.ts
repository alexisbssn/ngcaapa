import { Component, OnInit, Directive } from '@angular/core';
import { ChartsService } from '../../../../DAL/charts.service'
import { InterventionsPerTheme_DTO } from '../../../../DTO/InterventionsPerTheme_DTO'
import { ChartColors } from '../ChartColors'
import { UIChart} from 'primeng/primeng';

@Component({
  selector: 'app-chart-intervention-per-theme',
  templateUrl: './chart-intervention-per-theme.component.html',
  styleUrls: ['./chart-intervention-per-theme.component.css']
})
export class ChartInterventionPerThemeComponent implements OnInit {

  data:any
  message:string
  display:boolean = false;

  labels: string[] = []
  values: number[] = []
  colors: string[] = []

  constructor(public dataService : ChartsService) {
    this.message = 'Un instant, vos données sont en téléchargement...';
    this.data = {
      labels: this.labels,
      datasets: [
         {
            data: this.values,
            backgroundColor:this.colors
         }
      ]
    }
  }

  ngOnInit() {
    this.dataService.getInterventionPerTheme().subscribe(
      (data) => this.formatResult(data));
  }

  formatResult(result) {

    if (result.length > 0){
      for (var i = 0; i < result.length; i++)
      {
          this.labels[i] = result[i].theme.label;
          this.values[i] = result[i].numberOfInterventions;

          if (i <= ChartColors.colors.arrayColors.length){
            this.colors[i] = ChartColors.colors.arrayColors[i];
          }
      }
      this.display = true;
      this.message = 'Vos données sont prêtes!';
    }
    else
    {
      this.message = 'Aucune donnée trouvée!'
    }
  }

  refreshData(chart:UIChart){
    this.message = '';
    chart.refresh();
  }
}
