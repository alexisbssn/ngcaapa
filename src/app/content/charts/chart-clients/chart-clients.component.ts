import { Component, OnInit, Directive} from '@angular/core';
import { ChartsService } from '../../../../DAL/charts.service'
import { Client_DTO } from '../../../../DTO/Client_DTO'
import { Client } from '../../../../model/Client'

@Component({
  selector: 'app-chart-clients',
  templateUrl: './chart-clients.component.html',
  styleUrls: ['./chart-clients.component.css']
})
export class ChartClientsComponent implements OnInit {

  ClientName: string
  TotalHours: number
  TotalInterventions: number
  display:boolean = false;

  constructor(public dataService : ChartsService) { }

  ngOnInit() {
    this.dataService.getClientInformationByID(21).subscribe(
      (data) => this.formatResult(data));
  }
  
  formatResult(result) {
    this.ClientName = result.client.contact.firstName + " " + result.client.contact.lastName;
    this.TotalHours = result.hoursOfInterventions;
    this.TotalInterventions = result.numberOfInterventions;
    this.display = true;
  }

}
