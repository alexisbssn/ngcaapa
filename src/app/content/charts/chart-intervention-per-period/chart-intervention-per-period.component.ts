import { Component, OnInit, Directive } from '@angular/core';
import { ChartsService } from '../../../../DAL/charts.service'
import { InterventionsPerPeriod_DTO } from '../../../../DTO/InterventionsPerPeriod_DTO'
import { ChartColors } from '../ChartColors';
import {ChartModule,UIChart,SelectItem,DropdownModule} from 'primeng/primeng'
import {Observable} from 'rxjs/Rx';

@Component({
  selector: 'app-chart-intervention-per-period',
  templateUrl: './chart-intervention-per-period.component.html',
  styleUrls: ['./chart-intervention-per-period.component.css']
})
export class ChartInterventionPerPeriodComponent implements OnInit {

  yearDataLoaded:boolean = false;
  trimesterDataLoaded:boolean = false;
  monthDataLoaded:boolean = false;
  weekDataLoaded:boolean = false;

  periodError:string = "";
  message:string;
  dataLabel:string;

  data:any;
  emptyLabels:string[] = [];
  emptyValues:number[] = [];

  periods: SelectItem[];

  selectedPeriod: string;

  yearLabels:string[] = [];
  yearValues:number[] = [];

  TrimesterLabels:string[] = [];
  TrimesterValues:number[] = [];

  monthLabels:string[] = [];
  monthValues:number[] = [];

  weekLabels:string[] = [];
  weekValues:number[] = [];

  constructor(public dataService : ChartsService) {

    this.message = 'Un instant, vos données sont en téléchargement...'

    this.periods = [];
    this.periods.push({label:'Selectionner une période', value:null});
    this.periods.push({label:'Année', value:"1"});
    this.periods.push({label:'trimestre', value:"2"});
    this.periods.push({label:'Mois', value:"3"});
    this.periods.push({label:'Semaine', value:"4"});

    this.data = {
      labels: this.emptyLabels,
      datasets: [
         {
            label: '',
            data: this.emptyValues,
            backgroundColor: '#00ccff'
         }
      ]
    }
  }

  ngOnInit() {

    this.dataService.getInterventionPerPeriod('1').subscribe(
      (data) =>{this.formatResult(data,1)})

    this.dataService.getInterventionPerPeriod('2').subscribe(
      (data) =>{this.formatResult(data,2)})

    this.dataService.getInterventionPerPeriod('3').subscribe(
      (data) =>{this.formatResult(data,3)})

    this.dataService.getInterventionPerPeriod('4').subscribe(
      (data) =>{this.formatResult(data,4)})
  }

  formatResult(result,periodType:number) {
    if (result.periods.length > 0){
      for (var i = 0; i < result.periods.length; i++)
      {
        switch (periodType){
          case 1:
            this.yearLabels[i] = result.periods[i].period;
            this.yearValues[i] = result.periods[i].numberOfInterventions;
          break;

          case 2:
            this.TrimesterLabels[i] = result.periods[i].period;
            this.TrimesterValues[i] = result.periods[i].numberOfInterventions;
          break;

          case 3:
            this.monthLabels[i] = result.periods[i].period;
            this.monthValues[i] = result.periods[i].numberOfInterventions;
          break;

          case 4:
            this.weekLabels[i] = result.periods[i].period;
            this.weekValues[i] = result.periods[i].numberOfInterventions;
          break;
        }
      }

      switch(periodType){
        case 1:
          this.yearDataLoaded = true;
        break;

        case 2:
          this.trimesterDataLoaded = true;
        break;

        case 3:
          this.monthDataLoaded = true;
        break;

        case 4:
          this.weekDataLoaded = true;
        break;
      }

      if (this.yearDataLoaded && this.trimesterDataLoaded && this.monthDataLoaded && this.weekDataLoaded){
        this.message = 'Vos données sont prêtes!';
      }
    }
    else{

      if (this.periodError == ""){
        this.periodError += ",";
      }

      switch(periodType){
        case 1:
          this.periodError += 'année';
        break;

        case 2:
          this.periodError += 'trimestre';
        break;

        case 3:
          this.periodError += 'mois';
        break;

        case 4:
          this.periodError += 'semaine';
        break;
      }
      this.message = 'Aucune donnée trouvée pour la/les période(s) : '+this.periodError;
    }
  }

  refreshData(chart:UIChart){

    this.message = ''

      switch (this.selectedPeriod){
        case "1":
         this.setData(this.yearLabels,'Nombre d\'interventions par année',this.yearValues);
        break;

        case "2":
          this.setData(this.TrimesterLabels,'Nombre d\'interventions par trimestre',this.TrimesterValues);
        break;

        case "3":
          this.setData(this.monthLabels,'Nombre d\'interventions par mois',this.monthValues);
        break;

        case "4":
          this.setData(this.weekLabels,'Nombre d\'interventions par semaine',this.weekValues);
        break;

        default:
          this.setData(this.emptyLabels,'',this.emptyValues);
        this.message = 'Choisissez une période';
    }

    chart.refresh()
  }

  setData(labels:string[],dataLabels:string,values:number[]){

    this.data = {
      labels: labels,
      datasets: [
         {
            label: dataLabels,
            data: values,
            backgroundColor: '#00ccff'
         }
      ]
    }
  }
}
