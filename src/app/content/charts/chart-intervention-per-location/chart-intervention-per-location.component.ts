import { Component, OnInit,ElementRef,Directive } from '@angular/core';
import { ChartsService } from '../../../../DAL/charts.service';
import { InterventionsPerLocationType_DTO } from '../../../../DTO/InterventionsPerLocationType_DTO';
import { InterventionType } from "../../../../model/InterventionType";
import { ChartColors } from '../ChartColors';
import { UIChart,Button } from 'primeng/primeng'

@Component({
  selector: 'app-chart-intervention-per-location',
  templateUrl: './chart-intervention-per-location.component.html',
  styleUrls: ['./chart-intervention-per-location.component.css']
})
export class ChartInterventionPerLocationComponent implements OnInit {

  data: any;
  message:string;
  display:boolean = false;

  labels: string[] = [];
  values: number[] = [];
  colors: string[] = [];

  constructor(public dataService : ChartsService) {

    this.message = 'Un instant, vos données sont en téléchargement...'
    this.data = {
      labels: this.labels,
      datasets: [
         {
            data: this.values,
            backgroundColor:this.colors
         }
      ]
    }
  }

  ngOnInit() {
    this.dataService.getInterventionPerLocation().subscribe(
      (data) => this.formatResult(data));
  }

  formatResult(result) {

    if (result.length > 0){
      for (var i = 0; i < result.length; i++)
      {
        this.labels[i] = result[i].interventionType.lieu;
        this.values[i] = result[i].numberOfInterventions;

        if (i <= ChartColors.colors.arrayColors.length){
          this.colors[i] = ChartColors.colors.arrayColors[i];
        }
      }
      this.display = true;
      this.message = 'Vos données sont prêtes!';
    }
    else
    {
      this.message = 'Aucune donnée trouvée!';
    }
  }

  refreshData(chart:UIChart){
    this.message = ''
    chart.refresh();
  }
}
