import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContactService } from '../../../../DAL/contact-service/contact.service';
import { Contact } from '../../../../model/Contact';

@Component({
  selector: 'app-contact-details',
  templateUrl: './contact-details.component.html',
  styleUrls: ['./contact-details.component.css']
})
export class ContactDetailsComponent implements OnInit {

  contact: Contact;

  constructor(private route: ActivatedRoute, private dao: ContactService) { }

  // TODO: if the contact is a Client/Ressource, redirect to the appropriate page
  ngOnInit() {
    this.route.params.subscribe(params => {
       var id = +params['id']; // (+) converts string 'id' to a number
       this.dao.load(id).subscribe(response => {
         this.contact = response;
       });
    });
  }

}
