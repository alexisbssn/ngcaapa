import { Component, OnInit, Input } from '@angular/core';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { Contact } from '../../../../model/Contact';
import { ContactService } from '../../../../DAL/contact-service/contact.service';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  cols;
  contacts: Array<Contact>;
  constructor(private dao: ContactService) { }

  ngOnInit() {
    this.dao.list().subscribe(response => {
      this.contacts = response;
    });

    this.cols = [
      {field: 'id',   header: 'ID'},
      {field: 'lastName',   header: 'Nom'},
      {field: 'firstName',  header: 'Prenom'},
      {field: 'address',    header: 'Adresse'},
      {field: 'phone',      header: 'Telephone'},
      {field: 'phone2',     header: 'Telephone2'},
      {field: 'email',      header: 'Email'}
    ];

  }


  save() {

  }

  delete() {

  }
}
