import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { AutoCompleteModule, DropdownModule, SelectItem } from 'primeng/primeng';
import { Contact } from '../../../../model/Contact';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {
  @Output() contactUpdated = new EventEmitter<Contact>()

  newContact : Contact;
  constructor() {
    this.newContact = new Contact();
    this.contactUpdated.emit(this.newContact);
  }

  ngOnInit() {
  }

  toCreateContact(){


  }
}
