import { Component, OnInit, Input } from '@angular/core';
import { Contact } from '../../../../model/Contact';

@Component({
  selector: 'app-contact-details-content',
  templateUrl: './contact-details-content.component.html',
  styleUrls: ['./contact-details-content.component.css']
})
export class ContactDetailsContentComponent implements OnInit {

  @Input() contact: Contact;

  constructor() { }

  ngOnInit() {
  }

}
