import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { HttpModule, JsonpModule } from '@angular/http';

import {
  ConfirmationService,
  ConfirmDialogModule,
  DropdownModule,
  ScheduleModule,
  DataTableModule,
  SharedModule,
  ChartModule,
  DialogModule,
  CalendarModule,
  AutoCompleteModule
} from 'primeng/primeng';

import { EqualValidator } from './equal-validator.directive';
import { AppComponent } from './app.component';

import { ChartInterventionPerLocationComponent } from './content/charts/chart-intervention-per-location/chart-intervention-per-location.component';
import { ChartInterventionPerRegionComponent } from './content/charts/chart-intervention-per-region/chart-intervention-per-region.component';
import { ChartInterventionPerThemeComponent } from './content/charts/chart-intervention-per-theme/chart-intervention-per-theme.component';
import { ChartInterventionPerPeriodComponent } from './content/charts/chart-intervention-per-period/chart-intervention-per-period.component';
import { ChartClientsComponent } from './content/charts/chart-clients/chart-clients.component'
import { ChartsComponent } from './content/charts/charts/charts.component';

import { HomepageComponent } from './content/homepage/homepage.component';
import { LoginPageComponent } from './login-page/login-page.component';

import { HeaderComponent } from './partial/header/header.component';
import { NavBarComponent } from './partial/navbar/navbar.component';
import { FooterComponent } from './partial/footer/footer.component';
import { HeaderLinksStatistiquesComponent } from './partial/header-links-statistiques/header-links-statistiques.component';

import { CreateComponent } from './create/create.component';
import { DeleteComponent } from './delete/delete.component';

import { DemandeComponent } from './content/demande/demande-list/demande.component';
import { CreateEditDemandeComponent } from './content/demande/create-edit-demande/create-edit-demande.component';
import { DemandeDetailsComponent } from './content/demande/demande-details/demande-details.component';

import { AssignedListComponent } from './content/assigned-list/assigned-list.component'
import { MyDemandesComponent } from './content/my-demandes/my-demandes.component';
import { MyInterventionsComponent } from './content/my-interventions/my-interventions.component';


import { RegionCreateComponent } from './content/region/region-create/region-create.component';
import { RegionUpdateComponent } from './content/region/region-update/region-update.component';
import { RegionReadComponent } from './content/region/region-read/region-read.component';
import { RegionListComponent } from './content/region/region-list/region-list.component';

import { RessourceTypeCreateComponent } from './content/ressourceType/ressource-type-create/ressource-type-create.component';
import { RessourceTypeReadComponent } from './content/ressourceType/ressource-type-read/ressource-type-read.component';
import { RessourceTypeUpdateComponent } from './content/ressourceType/ressource-type-update/ressource-type-update.component';
import { RessourceTypeListComponent } from './content/ressourceType/ressource-type-list/ressource-type-list.component';

import { InterventionTypeCreateComponent } from './content/interventionType/intervention-type-create/intervention-type-create.component';
import { InterventionTypeReadComponent } from './content/interventionType/intervention-type-read/intervention-type-read.component';
import { InterventionTypeUpdateComponent } from './content/interventionType/intervention-type-update/intervention-type-update.component';
import { InterventionTypeListComponent } from './content/interventionType/intervention-type-list/intervention-type-list.component';

import { DocumentCreateComponent } from './content/document/document-create/document-create.component';
import { DocumentReadComponent } from './content/document/document-read/document-read.component';
import { DocumentUpdateComponent } from './content/document/document-update/document-update.component';
import { DocumentListComponent } from './content/document/document-list/document-list.component';

import { ThemeCreateComponent } from './content/theme/theme-create/theme-create.component';
import { ThemeReadComponent } from './content/theme/theme-read/theme-read.component';
import { ThemeUpdateComponent } from './content/theme/theme-update/theme-update.component';
import { ThemeListComponent } from './content/theme/theme-list/theme-list.component';

import { ClientCreateComponent } from './content/client/client-create/client-create.component';
import { ClientUpdateComponent } from './content/client/client-update/client-update.component';
import { ClientEditComponent } from './content/client/client-edit/client-edit.component';
import { ClientListComponent } from './content/client/client-list/client-list.component';
import { ClientDetailsComponent } from './content/client/client-details/client-details.component';

import { ContactEditComponent } from './content/contact/contact-edit/contact-edit.component';
import { ContactUpdateComponent } from './content/contact/contact-update/contact-update.component';
import { ContactCreateComponent } from './content/contact/contact-create/contact-create.component';
import { ContactListComponent } from './content/contact/contact-list/contact-list.component';
import { ContactDetailsComponent } from './content/contact/contact-details/contact-details.component';
import { ContactDetailsContentComponent } from './content/contact/contact-details-content/contact-details-content.component';

import { RessourceCreateComponent } from './content/ressource/ressource-create/ressource-create.component';
import { RessourceEditComponent } from './content/ressource/ressource-edit/ressource-edit.component';
import { RessourceUpdateComponent } from './content/ressource/ressource-update/ressource-update.component';
import { RessourceListComponent } from './content/ressource/ressource-list/ressource-list.component';

import { ReferenceCreateComponent } from './content/reference/reference-create/reference-create.component';
import { ReferenceListComponent } from './content/reference/reference-list/reference-list.component';
import { ReferenceReadComponent } from './content/reference/reference-read/reference-read.component';
import { ReferenceUpdateComponent } from './content/reference/reference-update/reference-update.component';


import { ProblemeComponent } from './content/probleme/probleme-list/probleme.component';
import { CreateEditProblemeComponent } from './content/probleme/create-edit-probleme/create-edit-probleme.component';
import { ProblemeDetailsComponent } from './content/probleme/probleme-details/probleme-details.component';

import { InterventionComponent } from './content/intervention/intervention.component';
import { InterventionUpdateComponent } from './content/intervention/intervention-update/intervention-update.component';
import { InterventionCreateComponent } from './content/intervention/intervention-create/intervention-create.component';
import { InterventionReadComponent } from './content/intervention/intervention-read/intervention-read.component';

import { DocumentService } from '../DAL/document-service/document.service';
import { InterventionTypeService } from '../DAL/interventionType-service/intervention-type.service';
import { ReferenceService } from '../DAL/reference-service/reference.service';
import { RegionService } from '../DAL/region-service/region.service';
import { RessourceTypeService } from '../DAL/ressourceType-service/ressource-type.service';
import { ThemeService } from '../DAL/theme-service/theme.service';
import { WsUrlProviderService } from "../DAL/ws-url-provider.service";
import { InterventionService } from "../DAL/intervention.service";
import { ChartsService } from '../DAL/charts.service'
import { DemandeService } from "../DAL/demande.service";
import { ContactService } from '../DAL/contact-service/contact.service';
import { RessourceService } from '../DAL/ressource-service/ressource.service';
import { ClientService } from '../DAL/client-service/client.service';
import { ProblemeService } from '../DAL/probleme.service';

import { routing } from './app.routes';


import { InMemoryDAO } from '../DAL/InMemoryDAO';


@NgModule({
  declarations: [
    AppComponent,
    EqualValidator,

    LoginPageComponent,
    HomepageComponent,

    DeleteComponent,
    CreateComponent,

    DemandeComponent,
    CreateEditDemandeComponent,
    DemandeDetailsComponent,

    ChartInterventionPerLocationComponent,
    ChartInterventionPerRegionComponent,
    ChartInterventionPerThemeComponent,
    ChartInterventionPerPeriodComponent,
    ChartClientsComponent,
    ChartsComponent,

    HeaderComponent,
    NavBarComponent,
    FooterComponent,
    HeaderLinksStatistiquesComponent,

    AssignedListComponent,
    MyInterventionsComponent,
    MyDemandesComponent,

    RegionCreateComponent,
    RegionUpdateComponent,
    RegionReadComponent,
    RegionListComponent,

    RessourceTypeCreateComponent,
    RessourceTypeReadComponent,
    RessourceTypeUpdateComponent,
    RessourceTypeListComponent,

    InterventionTypeCreateComponent,
    InterventionTypeReadComponent,
    InterventionTypeUpdateComponent,
    InterventionTypeListComponent,

    DocumentCreateComponent,
    DocumentReadComponent,
    DocumentUpdateComponent,
    DocumentListComponent,

    ThemeCreateComponent,
    ThemeReadComponent,
    ThemeUpdateComponent,
    ThemeListComponent,

    ClientCreateComponent,
    ClientUpdateComponent,
    ClientEditComponent,
    ClientListComponent,
    ClientDetailsComponent,

    ContactEditComponent,
    ContactUpdateComponent,
    ContactCreateComponent,
    ContactListComponent,
    ContactDetailsComponent,
    ContactDetailsContentComponent,

    RessourceCreateComponent,
    RessourceEditComponent,
    RessourceUpdateComponent,
    RessourceListComponent,

    ReferenceCreateComponent,
    ReferenceListComponent,
    ReferenceReadComponent,
    ReferenceUpdateComponent,

    InterventionComponent,
    InterventionUpdateComponent,
    InterventionCreateComponent,
    InterventionReadComponent,

    ProblemeComponent,
    CreateEditProblemeComponent,
    ProblemeDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    DropdownModule,
    AutoCompleteModule,
    ScheduleModule,
    DataTableModule,
    SharedModule,
    DialogModule,
    CalendarModule,
    AutoCompleteModule,
    ConfirmDialogModule,
    ChartModule,
    JsonpModule
    ],
  providers: [
    ChartsService,
    WsUrlProviderService,
    InterventionService,
    ConfirmationService,
    DocumentService,
    InterventionTypeService,
    ReferenceService,
    RegionService,
    RessourceTypeService,
    ThemeService,
    ContactService,
    RessourceService,
    ClientService,
    DemandeService,
    ProblemeService,
    InMemoryDAO
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
