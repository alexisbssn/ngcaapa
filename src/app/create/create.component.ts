import { Component, OnInit, Input } from '@angular/core';
import { Intervention } from '../../model/Intervention';
import { Client } from '../../model/Client';
import { Contact } from '../../model/Contact';
import { Ressource } from '../../model/Ressource';
import { ClientService } from '../../DAL/client-service/client.service';
import { ContactService } from '../../DAL/contact-service/contact.service';
import { RessourceService } from '../../DAL/ressource-service/ressource.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
})
export class CreateComponent implements OnInit {

  @Input() service: any
  @Input() ClientService: any
  @Input() ContactService: any
  @Input() RessourceService: any

  displayDialog: boolean = false;
  //service: any;

  constructor() {

  }

  ngOnInit() {

  }

  showDialogToAdd() {
    this.displayDialog = true
  }

  cancel(){
    // reset object
    this.displayDialog = false
  }

  save(data: any){
    this.service.create(data);
  }

  saveClient(data: any){
    this.ClientService.create(data);
  }
  saveRessource(data: any){
    this.RessourceService.create(data);
  }
  saveContact(data: any){
    this.ContactService.create(data);
  }
}
