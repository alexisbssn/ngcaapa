import { Component, Output } from '@angular/core';

/*import { Demande } from '../model/Demande'
import { Client } from '../model/Client'*/

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {


  @Output() apptitle = 'Caapa';

  title = 'Bonjour!';

  isLoggedOn = false;

  onLogin(bool : boolean){
    this.isLoggedOn = bool;
  }

  constructor(){

  }
}
