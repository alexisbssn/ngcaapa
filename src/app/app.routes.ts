import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component'

import { HomepageComponent } from './content/homepage/homepage.component';

import { ClientEditComponent } from './content/client/client-edit/client-edit.component';
import { ClientUpdateComponent } from './content/client/client-update/client-update.component';
import { ClientCreateComponent } from './content/client/client-create/client-create.component';
import { ClientDetailsComponent } from './content/client/client-details/client-details.component';
import { ClientListComponent } from './content/client/client-list/client-list.component';
import { RessourceTypeListComponent } from './content/ressourceType/ressource-type-list/ressource-type-list.component';
import { RessourceEditComponent } from './content/ressource/ressource-edit/ressource-edit.component';
import { RessourceUpdateComponent } from './content/ressource/ressource-update/ressource-update.component';
import { RessourceListComponent } from './content/ressource/ressource-list/ressource-list.component';
import { RessourceCreateComponent } from './content/ressource/ressource-create/ressource-create.component';
import { ContactEditComponent } from './content/contact/contact-edit/contact-edit.component';
import { ContactDetailsComponent } from './content/contact/contact-details/contact-details.component';
import { ContactListComponent } from './content/contact/contact-list/contact-list.component';
import { ContactCreateComponent } from './content/contact/contact-create/contact-create.component';
import { ContactUpdateComponent } from './content/contact/contact-update/contact-update.component';
import { DemandeComponent } from './content/demande/demande-list/demande.component';
import { DemandeDetailsComponent } from './content/demande/demande-details/demande-details.component';
import { CreateEditDemandeComponent } from './content/demande/create-edit-demande/create-edit-demande.component';
import { MyDemandesComponent } from './content/my-demandes/my-demandes.component';
import { RegionListComponent } from './content/region/region-list/region-list.component';
import { ReferenceListComponent } from './content/reference/reference-list/reference-list.component';
import { ThemeListComponent} from './content/theme/theme-list/theme-list.component';
import { DocumentListComponent } from './content/document/document-list/document-list.component';
import { InterventionComponent} from './content/intervention/intervention.component';
import { MyInterventionsComponent } from './content/my-interventions/my-interventions.component';
import { InterventionTypeListComponent } from './content/interventionType/intervention-type-list/intervention-type-list.component';
import { ChartInterventionPerLocationComponent } from './content/charts/chart-intervention-per-location/chart-intervention-per-location.component';
import { ChartInterventionPerRegionComponent } from './content/charts/chart-intervention-per-region/chart-intervention-per-region.component';
import { ChartInterventionPerThemeComponent } from './content/charts/chart-intervention-per-theme/chart-intervention-per-theme.component';
import { ChartInterventionPerPeriodComponent } from './content/charts/chart-intervention-per-period/chart-intervention-per-period.component';
import { ChartClientsComponent } from './content/charts/chart-clients/chart-clients.component'
import { ChartsComponent } from './content/charts/charts/charts.component'
import { DocumentReadComponent } from './content/document/document-read/document-read.component';
import { InterventionTypeReadComponent } from './content/interventionType/intervention-type-read/intervention-type-read.component';
import { RessourceTypeReadComponent } from './content/ressourceType/ressource-type-read/ressource-type-read.component';
import { ReferenceReadComponent } from './content/reference/reference-read/reference-read.component';
import { RegionReadComponent } from './content/region/region-read/region-read.component';
import { ThemeReadComponent } from './content/theme/theme-read/theme-read.component';

import { ProblemeComponent } from './content/probleme/probleme-list/probleme.component';
import { CreateEditProblemeComponent } from './content/probleme/create-edit-probleme/create-edit-probleme.component';
import { ProblemeDetailsComponent } from './content/probleme/probleme-details/probleme-details.component';

export const routes : Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomepageComponent },
  { path: 'clientCreate', component: ClientCreateComponent },
  { path: 'clientUpdate', component: ClientUpdateComponent },
  { path: 'clientEdit', component: ClientEditComponent },
  { path: 'client', component: ClientListComponent },
  { path: 'clientDetails/:id', component: ClientDetailsComponent },
  { path: 'contact', component: ContactListComponent },
  { path: 'contactDetails/:id', component: ContactDetailsComponent },
  { path: 'contactCreate', component: ContactCreateComponent },
  { path: 'contactUpdate', component: ContactUpdateComponent },
  { path: 'contactEdit', component: ContactEditComponent },
  { path: 'ressource', component: RessourceListComponent },
  { path: 'ressourceCreate', component: RessourceCreateComponent },
  { path: 'ressourceUpdate', component: RessourceUpdateComponent },
  { path: 'ressourceEdit', component: RessourceEditComponent },
  { path: 'typeressource', component: RessourceTypeListComponent },
  { path: 'demande', component: DemandeComponent },
  { path: 'demandeCreate', component: CreateEditDemandeComponent },
  { path: 'demandeCreate/:id', component: CreateEditDemandeComponent },
  { path: 'demandeDetails', component: DemandeDetailsComponent },
  { path: 'demandeDetails/:id', component: DemandeDetailsComponent },
  { path: 'evenement', component: ProblemeComponent },
  { path: 'problemeCreate', component: CreateEditProblemeComponent },
  { path: 'problemeCreate/:id', component: CreateEditProblemeComponent },
  { path: 'problemeDetails', component: ProblemeDetailsComponent },
  { path: 'problemeDetails/:id', component: ProblemeDetailsComponent },
  { path: 'mydemandes', component: MyDemandesComponent },
  { path: 'region', component: RegionListComponent },
  { path: 'reference', component: ReferenceListComponent},
  { path: 'theme', component: ThemeListComponent },
  { path: 'document', component: DocumentListComponent },
  { path: 'intervention', component: InterventionComponent },
  { path: 'typeintervention', component: InterventionTypeListComponent },
  { path: 'myinterventions', component: MyInterventionsComponent },
  { path: 'type-intervention', component: InterventionTypeListComponent },
  { path: 'chart-intervention-per-location', component: ChartInterventionPerLocationComponent},
  { path: 'chart-intervention-per-region', component: ChartInterventionPerRegionComponent},
  { path: 'chart-intervention-per-theme', component: ChartInterventionPerThemeComponent},
  { path: 'chart-intervention-per-period', component: ChartInterventionPerPeriodComponent},
  { path: 'chart-clients', component: ChartClientsComponent},
  { path: 'charts', component: ChartsComponent},
  { path: 'documents', component: DocumentListComponent },
  { path: 'interventions', component: InterventionComponent },
  { path: 'documentReads/:id', component: DocumentReadComponent },
  { path: 'interventionTypeReads/:id', component: InterventionTypeReadComponent },
  { path: 'referenceReads/:id', component: ReferenceReadComponent },
  { path: 'regionReads/:id', component: RegionReadComponent },
  { path: 'ressourceTypeReads/:id', component: RessourceTypeReadComponent },
  { path: 'themeReads/:id', component: ThemeReadComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
