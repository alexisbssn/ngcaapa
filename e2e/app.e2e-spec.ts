import { NgcaapaPage } from './app.po';

describe('ngcaapa App', function() {
  let page: NgcaapaPage;

  beforeEach(() => {
    page = new NgcaapaPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
