# Ngcaapa

* [Twitter Bootstrap] - great UI boilerplate for modern web apps
* [http://www.primefaces.org/primeng/] - PrimeNg - UI for Angular 2

### Installation

### Creating a branch

`git checkout -b maBranche`
Ça va directement vous déplacer avec toutes vos modifications dans une nouvelle branche

### How to commit
faire du travail

`git status` n'importe quand pour voir on est rendu où

`git add *`

`git commit -m "mon message de commit"`

`git push`


### How to get new modifications from master 
`git checkout master`

`git pull`

`git checkout maBranche`

`git merge master`

`git status`

Ou simplement  `git pull` si vous êtes déjà sur le `master`

### How to merge a branch on master 
`git checkout master`

`git pull`

`git merge maBranche`

`git status`

* Résoudre les potentiels conflits
* TESTER l'application
* Faire un commit comme d'habitude, en mettant comme message `Merge branch 'maBranche' into 'master'`


##Documentation Angular

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.19-3.

### Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

### Deploying to Github Pages

Run `ng github-pages:deploy` to deploy to Github Pages.

### Further help

To get more help on the `angular-cli` use `ng --help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>